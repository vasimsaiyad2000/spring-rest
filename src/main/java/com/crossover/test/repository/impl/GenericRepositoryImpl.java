package com.crossover.test.repository.impl;


import java.io.Serializable;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.crossover.test.repository.GenericRepository;


public abstract class GenericRepositoryImpl<T, ID extends Serializable> implements GenericRepository<T, ID> {

	private static Logger log = Logger.getLogger(GenericRepositoryImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	private Class<T> clazz;
	
	protected GenericRepositoryImpl(Class<T> clazz){
		this.clazz = clazz;
	}

	public T save(T entity) {
		
		try{
			getSession().save(entity);
		}catch(Exception e){
			entity = null;
		}
		
		return entity;
	}

	public T update(T entity) {
		
		try{
			getSession().update(entity);
		}catch(Exception e){
			entity = null;
		}
		
		return entity;
	}

	
	public T saveOrUpdate(T entity) {
		
		try{
			getSession().saveOrUpdate(entity);
		}catch(Exception e){
			entity = null;
		}
		
		return entity;
	}

	@SuppressWarnings("unchecked")
	public T findById(ID id) {
		return (T) getSession().get(this.clazz, id);
	}
			
	protected Session getSession(){
		return sessionFactory.getCurrentSession();
	}
	
	protected Criteria getCriteria(){
		return getSession().createCriteria(this.clazz);
	}
	
	@SuppressWarnings("unchecked")
	public T findOne(String field, String value){
		return (T) getSession().createCriteria(this.clazz)
				.add(Restrictions.eq(field, value)).uniqueResult();
	}
}

