package com.crossover.test.repository.impl;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.crossover.test.persistence.UserExam;
import com.crossover.test.repository.UserExamRepository;

@Repository
public class UserExamRepositoryImpl extends GenericRepositoryImpl<UserExam, Long> implements UserExamRepository{

	private static final String FIELD_USER_ID = "user.id";
	private static final String FIELD_EXAM_ID = "exam.id";
	
	public UserExamRepositoryImpl() {
		super(UserExam.class);
	}

	@Override
	public UserExam findByExamAndUser(Long examId, Long userId) {
		
		return (UserExam) getCriteria().add(Restrictions.eq(FIELD_EXAM_ID, examId))
					.add(Restrictions.eq(FIELD_USER_ID, userId))
					.uniqueResult();
	}
}
