package com.crossover.test.repository.impl;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Repository;

import com.crossover.test.persistence.Question;
import com.crossover.test.persistence.UserQuestionAnswer;
import com.crossover.test.repository.UserAnswerRepository;


@Repository
public class UserAnswerRepositoryImpl extends GenericRepositoryImpl<UserQuestionAnswer, Long> implements UserAnswerRepository{

	private static final String FIELD_USER_ID = "user.id";
	private static final String FIELD_QUESTION_ID = "question.id";
	private static final String FIELD_QUESTION = "question";
	private static final String FIELD_USER = "user";
	private static final String FIELD_CHOICE = "choice";
	private static final String FIELD_CHOICE_ISCORRECT = "choice.isCorrect";
	private static final String FIELD_EXAM_ID = "exam.id";
	private static final String FIELD_ID = "id";
	private static final String FIELD_POINT = "point";
	
	public UserAnswerRepositoryImpl() {
		super(UserQuestionAnswer.class);
	}

	@Override
	public UserQuestionAnswer findByUserAndQuestion(Long userId, Long questionId) {
		return (UserQuestionAnswer) getCriteria().add(Restrictions.eq(FIELD_USER_ID, userId))
					.add(Restrictions.eq(FIELD_QUESTION_ID, questionId))
					.uniqueResult();
	}

	@Override
	public Long findScoreByUser(Long userId, Long examId) {

		DetachedCriteria dCriteria = DetachedCriteria.forClass(UserQuestionAnswer.class);
		dCriteria.setProjection(Projections.property(FIELD_QUESTION_ID));
		dCriteria.add(Restrictions.eq(FIELD_USER_ID, userId));
		
		dCriteria.createAlias(FIELD_USER, FIELD_USER, JoinType.INNER_JOIN);
		dCriteria.createAlias(FIELD_QUESTION, FIELD_QUESTION, JoinType.INNER_JOIN);
		dCriteria.createAlias(FIELD_CHOICE, FIELD_CHOICE, JoinType.INNER_JOIN, 
				Restrictions.eq(FIELD_CHOICE_ISCORRECT, true));
		
		Criteria criteria = getSession().createCriteria(Question.class)
					.add(Restrictions.eq(FIELD_EXAM_ID, examId))
					.add(Subqueries.propertyIn(FIELD_ID, dCriteria))
					.setProjection(Projections.sum(FIELD_POINT));
				
		Long result = (Long) criteria.uniqueResult();
		return (result != null) ? result : 0; 
	}
}
