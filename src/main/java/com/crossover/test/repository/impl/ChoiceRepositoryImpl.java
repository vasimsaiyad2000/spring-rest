package com.crossover.test.repository.impl;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.crossover.test.persistence.Choice;
import com.crossover.test.repository.ChoiceRepository;

@Repository
public class ChoiceRepositoryImpl extends GenericRepositoryImpl<Choice, Long> implements ChoiceRepository {

	private static final String FIELD_QUESTION_ID = "question.id";
	
	public ChoiceRepositoryImpl() {
		super(Choice.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Choice> findByQuestion(Long questionId) {
		return getCriteria().add(Restrictions.eq(FIELD_QUESTION_ID, questionId)).list();
	}

}
