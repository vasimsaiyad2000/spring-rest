package com.crossover.test.repository.impl;

import org.springframework.stereotype.Repository;

import com.crossover.test.persistence.Exam;
import com.crossover.test.repository.ExamRepository;

@Repository
public class ExamRepositoryImpl extends GenericRepositoryImpl<Exam, Long> implements ExamRepository{

	public ExamRepositoryImpl() {
		super(Exam.class);
	}
}
