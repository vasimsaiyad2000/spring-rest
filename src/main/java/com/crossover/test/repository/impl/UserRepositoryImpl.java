package com.crossover.test.repository.impl;

import org.springframework.stereotype.Repository;

import com.crossover.test.persistence.User;
import com.crossover.test.repository.UserRepository;

@Repository
public class UserRepositoryImpl extends GenericRepositoryImpl<User, Long> implements UserRepository{

	private static final String FIELD_USER_EMAIL = "email";
	
	public UserRepositoryImpl() {
		super(User.class);
	}

	public User findByEmail(String email) {
		return findOne(FIELD_USER_EMAIL, email);
	}

}
