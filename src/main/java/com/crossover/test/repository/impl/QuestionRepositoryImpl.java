package com.crossover.test.repository.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Repository;

import com.crossover.test.persistence.Exam;
import com.crossover.test.persistence.Question;
import com.crossover.test.persistence.User;
import com.crossover.test.persistence.UserQuestionAnswer;
import com.crossover.test.repository.QuestionRepository;

@Repository
public class QuestionRepositoryImpl extends GenericRepositoryImpl<Question, Long> implements QuestionRepository {

	private static final String FIELD_QUESTION_ID = "id";
	private static final String FIELD_EXAM_ID = "exam.id";
	private static final String FIELD_CHOICE_COLLECTION = "choiceCollection";
	private static final String FIELD_USER_ANSWER_COLLECTION = "userQuestionAnswerCollection";
	private static final String FIELD_ANSWER_USER_ID = "user.id";
	private static final String FIELD_ANSWER_QUESTION = "question.id";
	private static final String FIELD_QUESTION_POINT = "point";
	
	
	public QuestionRepositoryImpl() {
		super(Question.class);
	}
	
	public Question findByExam(Long examId, Long userId, int offset) {
		
		Question q = (Question) getCriteria().add(Restrictions.eq(FIELD_EXAM_ID, examId))
						.addOrder(Order.asc(FIELD_QUESTION_ID))
						.createAlias(FIELD_USER_ANSWER_COLLECTION, FIELD_USER_ANSWER_COLLECTION, 
								JoinType.LEFT_OUTER_JOIN, Restrictions.eq(FIELD_ANSWER_USER_ID, userId))
						.setFirstResult(offset).setMaxResults(1).uniqueResult();
		
		Hibernate.initialize(q.getUserQuestionAnswerCollection());
		return q;
	}

	@Override
	public Question findById(Long id) {
		return (Question) getCriteria().add(Restrictions.eq(FIELD_QUESTION_ID, id))
				.createAlias(FIELD_CHOICE_COLLECTION, FIELD_CHOICE_COLLECTION, JoinType.LEFT_OUTER_JOIN)
				.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Question> findByExam(Long examId) {
		return getCriteria().add(Restrictions.eq(FIELD_EXAM_ID, examId))
					.addOrder(Order.asc(FIELD_QUESTION_ID)).list();
	}

	@Override
	public Long findCountByExam(Long examId) {
		return (Long) getCriteria().add(Restrictions.eq(FIELD_EXAM_ID, examId))				
				.setProjection(Projections.rowCount()).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Question> findUnAnsweredQuestions(Long examId, Long userId) {
		
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(UserQuestionAnswer.class)
				.add(Restrictions.eq(FIELD_ANSWER_USER_ID, userId))
				.setProjection(Projections.property(FIELD_ANSWER_QUESTION));
				
		return getCriteria().add(Restrictions.eq(FIELD_EXAM_ID, examId))
				.add(Subqueries.propertyNotIn(FIELD_QUESTION_ID, detachedCriteria))
				.list();
	}

	@Override
	public Long findTotalScoreByExam(Long examId) {
		return (Long) getCriteria().add(Restrictions.eq(FIELD_EXAM_ID, examId))
					.setProjection(Projections.sum(FIELD_QUESTION_POINT))
					.uniqueResult();
	}
}
