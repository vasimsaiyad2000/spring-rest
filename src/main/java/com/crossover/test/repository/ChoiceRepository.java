package com.crossover.test.repository;

import java.util.List;

import com.crossover.test.persistence.Choice;

/**
 * This interface is responsible to perform the database operations for question options.
 * @author vasim
 *
 */
public interface ChoiceRepository extends GenericRepository<Choice, Long>{

	/**
	 * Returns the list of options for given question
	 * @param questionId Given question id
	 * @return List Object
	 */
	public List<Choice> findByQuestion(Long questionId);
}

