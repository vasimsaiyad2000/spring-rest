package com.crossover.test.repository;

import com.crossover.test.persistence.Exam;

/**
 * This interface is responsible to perform the database operations for exam.
 * @author vasim
 *
 */
public interface ExamRepository extends GenericRepository<Exam, Long>{

}
