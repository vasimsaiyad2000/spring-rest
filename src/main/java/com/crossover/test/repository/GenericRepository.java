package com.crossover.test.repository;

import java.io.Serializable;
import java.util.List;

/**
 * This is a generice interface that define common database operations.
 * 
 * @author vasim
 *
 * @param <T>
 * @param <ID>
 */
public interface GenericRepository<T, ID extends Serializable> {

	public T save(T entity);
	public T update(T entity);
	public T saveOrUpdate(T entity);
	public T findById(ID id);
	public T findOne(String field, String value);

}