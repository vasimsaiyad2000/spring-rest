package com.crossover.test.utils;

import com.crossover.test.persistence.User;
/**
 * This is a class that is responsible to handle utilities operations
 * @author vasim
 *
 */
public class Utils {


	public static boolean validateUserPassword(User user, String password){
		return user.getPassword().equals(password);
	}
}
