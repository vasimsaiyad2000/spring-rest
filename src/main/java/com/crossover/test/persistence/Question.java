/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crossover.test.persistence;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.FetchMode;

/**
 * This is a persistence model class that mapped with question_master database table contains questions.
 * @author Vasim Saiyad
 */
@Entity
@Table(name = "question_master")
public class Question extends BaseEntity {
	
    private static final long serialVersionUID = 1L;
    
    @Column(name = "text")
    private String text;
    
    @Column(name = "point")
    private Long point;
    
    @JoinColumn(name = "exam_id", referencedColumnName = "id")
    @ManyToOne
    private Exam exam;
    
    @OneToMany(mappedBy = "question")
    private List<Choice> choiceCollection;
    
    @OneToMany(mappedBy = "question")
    private List<UserQuestionAnswer> userQuestionAnswerCollection;

    public Question() {
    }

    public Question(Long id) {
        super(id);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getPoint() {
        return point;
    }

    public void setPoint(Long point) {
        this.point = point;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

	public List<UserQuestionAnswer> getUserQuestionAnswerCollection() {
		return userQuestionAnswerCollection;
	}

	public void setUserQuestionAnswerCollection(List<UserQuestionAnswer> userQuestionAnswerCollection) {
		this.userQuestionAnswerCollection = userQuestionAnswerCollection;
	}

	public List<Choice> getChoiceCollection() {
		return choiceCollection;
	}

	public void setChoiceCollection(List<Choice> choiceCollection) {
		this.choiceCollection = choiceCollection;
	}
    
}
