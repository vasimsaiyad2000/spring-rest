package com.crossover.test.mapper;

import org.springframework.stereotype.Component;

import com.crossover.test.dto.UserAnswerDTO;
import com.crossover.test.persistence.UserQuestionAnswer;


/**
 * This is a mapper class that map UserAnswer data transportation object to UserQuestionAnwer persistence object.
 * @author vasim
 *
 */
@Component
public class UserAnswerMapper {

	public UserAnswerDTO convertToUserAnswerDTO(UserQuestionAnswer uqa){
		UserAnswerDTO userAnswerDTO = new UserAnswerDTO();
		
		userAnswerDTO.setChoiceId(uqa.getChoice().getId());
		userAnswerDTO.setQuestionId(uqa.getQuestion().getId());
		userAnswerDTO.setUserId(uqa.getUser().getId());
		
		return userAnswerDTO;
	}
}
