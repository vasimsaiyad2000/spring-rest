package com.crossover.test.mapper;

import org.springframework.stereotype.Component;

import com.crossover.test.dto.UserDTO;
import com.crossover.test.persistence.User;


/**
 * This is a mapper class that map User data transportation object to user persistence object.
 * @author vasim
 *
 */
@Component
public class UserMapper {

	public UserDTO convertToUserDTO(User user){
		UserDTO userDTO = new UserDTO();
		userDTO.setId(user.getId());
		userDTO.setFirstName(user.getFirstName());
		userDTO.setLastName(user.getLastName());
		userDTO.setEmail(user.getEmail());
		
		return userDTO;
	}
}
