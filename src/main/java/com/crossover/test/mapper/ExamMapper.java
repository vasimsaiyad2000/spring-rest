package com.crossover.test.mapper;

import org.springframework.stereotype.Component;

import com.crossover.test.dto.ExamDTO;
import com.crossover.test.persistence.Exam;

/**
 * This is a mapper class that map Exam data transportation object to exam persistence object.
 * @author vasim
 *
 */
@Component
public class ExamMapper {

	public ExamDTO convertToExamDTO(Exam exam){
		ExamDTO examDTO = new ExamDTO();
		
		examDTO.setId(exam.getId());
		examDTO.setTitle(exam.getTitle());
		examDTO.setDescription(exam.getDescription());
		examDTO.setActive(exam.isActive());
		examDTO.setDuration(exam.getDuration());
		
		return examDTO;
	}
}
