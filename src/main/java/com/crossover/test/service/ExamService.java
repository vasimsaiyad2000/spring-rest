package com.crossover.test.service;

import com.crossover.test.persistence.Exam;

/**
 * This interface define service layer operations for exam.
 * 
 * ExamServiceImpl class implements all operation for this interface.
 * 
 * @author vasim
 * 
 */
public interface ExamService {

	/**
	 * Get the exam for given id
	 * @param id Given id
	 * @return Exam Details
	 */
	public Exam getById(Long id);
}
