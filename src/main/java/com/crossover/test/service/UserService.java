package com.crossover.test.service;

import com.crossover.test.persistence.User;

/**
 * This interface define service layer operations for user.
 * 
 * UserServiceImpl class implements all operation for this interface.
 * 
 * @author vasim
 * 
 */
public interface UserService {

	/**
	 * Get the user by given email
	 * @param email Given email
	 * @return User object
	 */
	public User getByEmail(String email);
}
