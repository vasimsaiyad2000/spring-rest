package com.crossover.test.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crossover.test.persistence.Exam;
import com.crossover.test.repository.ExamRepository;
import com.crossover.test.service.ExamService;

@Service
public class ExamServiceImpl implements ExamService{

	@Autowired
	private ExamRepository examRepository;

	@Transactional
	public Exam getById(Long id) {
		return examRepository.findById(id);
	}
}
