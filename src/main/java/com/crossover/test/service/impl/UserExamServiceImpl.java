package com.crossover.test.service.impl;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crossover.test.persistence.Exam;
import com.crossover.test.persistence.User;
import com.crossover.test.persistence.UserExam;
import com.crossover.test.repository.UserExamRepository;
import com.crossover.test.service.UserExamService;

@Service
public class UserExamServiceImpl implements UserExamService{
	
	@Autowired
	private UserExamRepository userExamRepository;
	
	@Transactional
	public UserExam getByExamAndUser(Long examId, Long userId) {
		return userExamRepository.findByExamAndUser(examId, userId);
	}

	@Transactional
	public void saveUserExam(Long examId, Long userId) {
		UserExam userExam = new UserExam(new Exam(examId), new User(userId), new Date());
		userExamRepository.save(userExam);
	}

	@Transactional
	public UserExam saveUserScore(Long score, Long percentage, Long userId, Long examId) {
		
		UserExam userExam = this.getByExamAndUser(examId, userId);
		userExam.setScore(score);
		userExam.setPercentage(percentage);
		
		return userExamRepository.update(userExam);
	}
}
