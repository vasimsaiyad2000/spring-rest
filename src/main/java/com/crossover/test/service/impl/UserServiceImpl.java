package com.crossover.test.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crossover.test.persistence.User;
import com.crossover.test.repository.UserRepository;
import com.crossover.test.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Transactional
	public User getByEmail(String email) {
		return userRepository.findByEmail(email);
	}

}
