package com.crossover.test.exception;

import java.util.Set;

import javax.validation.ConstraintViolation;

import org.springframework.validation.Errors;

/**
 * This class is responsible to throw bad request exception.
 * <p> 
 * This class is mostly used to throw validation errors.
 * 
 * @author vasim
 *
 */
public class BadRequestException extends AppException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor
	 */
	public BadRequestException() {
	
	}

	/**
	 * Parameterized constructor
	 * @param cause Throwable object
	 */
	public BadRequestException(Throwable cause) {
		super(cause);
	}

	/**
	 * Parameterized constructor
	 * @param message exception message
	 */
	public BadRequestException(String message){
		super(message);
	}
	
	/**
	 * Parameterized constructor
	 * @param message Exception message
	 * @param cause Throwable object
	 */
	public BadRequestException(String message, Throwable cause) {
		super(message, cause);
	}
}
