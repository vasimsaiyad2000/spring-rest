<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet" href="app/css/bootstrap.css">
<link type="text/css" rel="stylesheet" href="app/css/style.css">
</head>
<body ng-controller = "AppCtrl">
<script type="text/javascript" src="app/lib/requirejs/require.min.js" data-main="app/main.js"></script>

<div ng-controller = "NavBarCtrl" ng-if ="isNavBar()">
	<ng-include src = "'app/view/navbar.html'" />
</div>
<div ng-view></div>
</body>
</html>
