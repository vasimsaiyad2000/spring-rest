'use strict';
define(['angularAMD'], function(angularAMD){
	angularAMD.controller("ResultCtrl", function($scope,$routeParams, $location, $timeout, $http, $filter, apiService, Timer){

		$scope.exam = $scope.getCurrentTest();
		$scope.user = $scope.getCurrentUser();
		$scope.result = {};
		
		$scope.init = function(){
			$scope.calculateResult();	
		};
		
		$scope.calculateResult = function(){
			var apiUrl = 'api/result/';
			var params = {'examId' : $scope.exam.id, 'userId': $scope.user.id};
			
			apiService.post(apiUrl, params, function(response){
				$scope.result = response;
			});
		};
	 });
});
