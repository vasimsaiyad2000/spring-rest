'use strict';
define(['angularAMD'], function(angularAMD){
	angularAMD.controller("HomeCtrl", function($scope,$routeParams, $location, $timeout, $http, $cookieStore, apiService){
		
		 $scope.exam = {};
		 $scope.user = $scope.getCurrentUser();
		 
		 $scope.init = function(){
			 $scope.loadTest();
		 };
		 
		 $scope.loadTest = function(){
			 apiService.get('api/exam/1', function(response){
					$scope.exam = response;
			 }); 
		 };
		 
		 $scope.startTest = function(){
			 var params = {'id' : $scope.exam.id, 'userId' : $scope.user.id}
			 
			 apiService.post('api/exam/start', params, function(response){
					$scope.setCurrentTest($scope.exam);

					$timeout(function(){
						$location.path('/start');
					}, 1000); 		
			});
		 };
	});
});
