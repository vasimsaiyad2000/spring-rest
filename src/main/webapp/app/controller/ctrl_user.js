'use strict';
define(['angularAMD'], function(angularAMD){
	angularAMD.controller("UserCtrl", function($scope, $routeParams, $location,$timeout,$interval, $http, apiService){
	
		$scope.user = {};
		$scope.duration = 3600;
		
		$scope.init = function(){
			$interval(function(){
				$scope.duration = $scope.duration - 1;
				$scope.hours = $scope.startClock();	
			}, 1000);
		};
		
		$scope.signin = function(){
			$scope.clearErrors();
			
			apiService.post('api/user/login', $scope.user, function(response){
				$scope.setCurrentUser(response);
				$scope.ok("You have loggedin successfully");
				
				$timeout(function(){
					$location.path('/');
				}, 2000);
		 		
			});
		 };
 
		 $scope.startClock = function(){
			 
			 var secs = Math.round($scope.duration);
			 var hours = Math.floor(secs / (60 * 60));

			 var divisor_for_minutes = secs % (60 * 60);
			 var minutes = Math.floor(divisor_for_minutes / 60);

			 var divisor_for_seconds = divisor_for_minutes % 60;
			 var seconds = Math.ceil(divisor_for_seconds);

			 var obj = {"h": hours, "m": minutes,"s": seconds};
			 return obj;
		 }
		 
		 $scope.clearErrors();
	});
});