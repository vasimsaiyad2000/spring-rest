'use strict';
define(['angularAMD'], function(angularAMD){
	angularAMD.controller("ExamCtrl", function($scope,$routeParams, $location, $timeout,$interval, $http, $filter, apiService, Timer){
		
		$scope.question = {};
		$scope.time = {};
		$scope.answer = {};
		$scope.questionId = 0;
		$scope.notAnswered = [];
		$scope.exam = $scope.getCurrentTest();
		$scope.user = $scope.getCurrentUser();
		$scope.duration = $scope.exam.duration;
		
		$scope.init = function(){
			
			$scope.nextQuestion = 1;
			$scope.currentQuestion = 1;
			$scope.lastQuestion = false;
			$scope.isFinished = false; 
			
			$scope.populateDropDown();
			$scope.loadQuestion();
			
			Timer.startTimer(function(){
				$scope.timer();
			});
			
			$interval(function(){
				$scope.duration = $scope.duration - 1;
				$scope.time = $scope.startClock();
				
			}, 1000);
		};
		
		$scope.populateDropDown = function(){
			
			$scope.questions = [];
			var apiUrl = 'api/question/list/' + $scope.exam.id;
			
			 apiService.get(apiUrl, function(response){
				 for(var i = 0; i < response.length; i++){
						$scope.questions.push(response[i].id);
				 }
			 }); 
		};
		
		$scope.loadQuestion = function(){
			
			//$scope.nextQuestion = ($scope.nextQuestion > 0) ? $scope.nextQuestion : 1;
			var apiUrl = 'api/question/';
			var params = {'examId' : $scope.exam.id, 'userId' : $scope.user.id, 'questionNumber' : $scope.nextQuestion};
			
			apiService.post(apiUrl, params, function(response){
				
					$scope.question = response;
					$scope.lastQuestion = response.lastQuestion;
					$scope.nextQuestion = response.nextQuestion;
					$scope.currentQuestion = response.currentQuestion;
					
					$scope.clearErrors();
			}); 
		};
		
		$scope.searchQuestion = function(id){
			var index = $scope.questions.indexOf(parseInt(id, 10));
			$scope.nextQuestion = index + 1;
			$scope.loadQuestion();
		};
		
		$scope.submitAnswer = function(option){
			
			$scope.clearErrors();
			
			var apiUrl = "api/answer/";
			var params = {'choiceId' : option, 'questionId' : $scope.question.id, 'userId' :$scope.user.id};
			
			apiService.post(apiUrl, params, function(response){
				$scope.answer = {};
				
				if(!$scope.lastQuestion){
					$scope.loadQuestion();
				}else{
					$scope.endTest();
				}
			});
		};

		$scope.timer = function(){
			var apiUrl = "api/timer/";
			var params = {'examId' : $scope.exam.id, 'userId' : $scope.user.id, 'duration' :$scope.exam.duration};
			
			apiService.post(apiUrl, params);
		};

		$scope.endTest = function(){
			$scope.isFinished = true;
			Timer.stopTimer();
			$scope.checkNotAnsweredQuestions();
		};
		
		$scope.checkNotAnsweredQuestions = function(){
			var apiUrl = 'api/question/unanswer/' + $scope.exam.id + "/" + $scope.user.id;
			
			apiService.get(apiUrl, function(response){
				$scope.notAnswered = response;
			});
		};
		
		$scope.confirmFinished = function(){
			$timeout(function(){
				$location.path('/result');
			}, 1000);
		};
		
		$scope.startClock = function(){
			 
			 var secs = Math.round($scope.duration);
			 var hours = Math.floor(secs / (60 * 60));

			 var divisor_for_minutes = secs % (60 * 60);
			 var minutes = Math.floor(divisor_for_minutes / 60);

			 var divisor_for_seconds = divisor_for_minutes % 60;
			 var seconds = Math.ceil(divisor_for_seconds);

			 var obj = {"h": hours, "m": minutes,"s": seconds};
			 return obj;
		 }
 
		$scope.clearErrors();
		
	 });
});
