require.config({
    baseUrl: "app",
    paths: {
        'angular': 'lib/angularjs/angular',
        'angularRoute': 'lib/angularjs/angular-route.min',
        'angularAnimate': 'lib/angularjs/angular-animate.min',
        'angularCookie': 'lib/angularjs/angular-cookies.min',
        'angularMessages': 'lib/angularjs/angular-messages.min',
        'angularAMD': 'lib/angularjs/angularAMD.min',
        'angularSanitize': 'lib/angularjs/angular-sanitize.min',
        'angularwizard': 'lib/angularjs/angular-wizard',
        'angularHttp' : 'lib/angularjs/angular-http-auth',
        'jquery': 'lib/jquery/jquery.min',
        'bootstrap' : 'lib/bootstrap/bootstrap.min',
        'app.service' : 'service/service',
        'app.timer' : 'service/timer'
    },

    waitSeconds: 0,
    shim: {
        'angularAMD': ['angular'],
        'angularRoute': ['angular'],
        'angularwizard': ['angular'],
        'tagmodule': ['angular'],
        'angularCookie': ['angular'],
        'angularMessages' : ['angular'],
        'angularAnimate': ['angular'],
        'angularSanitize': ['angular'],
        'angularHttp': ['angular'],
        'jquery': {
            exports: '$'
          },
        'bootstrap' : { 'deps' :['jquery'] },
        'app.service':['angular'],
        'app.timer':['angular']
    },
    deps: ['app']
});

