var service = angular.module('app.service', []);

service.service('apiService', function($rootScope, $http, $location, $timeout, Timer){
    
	this.get = function(apiUrl, callback){
		
		$http.get(apiUrl).success(function(response) {
			
			if(callback) callback(response);
			
		}).error(function(data, status){
			errorHandler(data, status);
		});
	}

	this.post = function(apiUrl, data, callback){
		
		$http.post(apiUrl, data).success(function(response) {
			
			if(callback) callback(response);
			
        }).error(function(data, status){
			errorHandler(data, status);
		});
	}

	var errorHandler = function(data, status){
		
		switch (status) {
		    case 400:
		    case 404:
		    case 500:
		    	$rootScope.error(data.message);
			    break;
		    case 403:
		    	alert(data.message);
		    	Timer.stopTimer();
		    	
		    	$timeout(function(){
		        	$location.path('/result');
		        }, 1000);
		    	break;
		    case 401:
		        $timeout(function(){
		        	$location.path('/signin');
		        }, 1000);
		        break;
		    default:
			    break;
		}
	}
});