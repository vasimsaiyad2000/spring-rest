var app = angular.module('app.timer', []);

app.factory('Timer', function($rootScope, $interval){

	var clock = null;
	  var service = {
	    
	 startTimer: function(fn){
		 
	      if(clock === null){
	        clock = $interval(fn, 2000);
	      }
	    },
	    
	    stopTimer: function(){
	    	
	      if(clock !== null){
	        $interval.cancel(clock);
	        clock = null;
	      }
	    }
	  };

	  return service;
});