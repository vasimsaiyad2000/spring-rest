'use strict';
define(['angularAMD',
    'angularRoute',
    'angularAnimate',
    'angularCookie',
    'angularSanitize',
    'angularMessages',
    'angularHttp',
    'jquery',
    'bootstrap',
    'app.service',
    'app.timer'
    ], function (angularAMD) {
        var app = angular.module("angulardemo", ['ngRoute', 'ngCookies','ngSanitize', 'ngMessages', 'app.service', 'app.timer']);
        app.config(function ($routeProvider, $httpProvider) {
            $routeProvider.when("/", angularAMD.route({
            	  templateUrl: 'app/view/home.html',
                  controller : 'HomeCtrl',
                  controllerUrl : 'controller/ctrl_home'
            }))
            .when("/signin", angularAMD.route({
                templateUrl: 'app/view/login.html',
                controller : 'UserCtrl',
                controllerUrl : 'controller/ctrl_user'
            }))
            .when("/start", angularAMD.route({
            	templateUrl: 'app/view/exam.html',
                controller : 'ExamCtrl',
                controllerUrl : 'controller/ctrl_exam'
            }))
            .when("/result", angularAMD.route({
                templateUrl: 'app/view/result.html',
                controller : 'ResultCtrl',
                controllerUrl : 'controller/ctrl_result'
            }))
        });
        
               
        app.controller("AppCtrl", function ($scope, $location) {
        	
        	var urls = ['/signin', '/signup'];
        	
        	$scope.isNavBar = function(){
        		return $.inArray($location.path(), urls) === -1;
        	};
        	
        	$scope.isBackGround = function(){
        		return !($.inArray($location.path(), urls) === -1);
        	};
        });
        
        app.controller("NavBarCtrl", function($scope, $location, $rootScope){
        	$scope.activePath = null;
        	
        	$scope.currentUser = function(){
        		var userCookie = $rootScope.getCurrentUser();
        		var displayName = "Guest";

            	if(!angular.isUndefined(userCookie)){
            		displayName = userCookie.firstName + " " + userCookie.lastName;
            	}
            	
            	return displayName;
        	};
        	
        	$scope.signout = function(){
        		$rootScope.removeCurrentUser();
        		$location.path('/signin');
        	};
        	        	 
        	$scope.$on('$locationChangeSuccess', function(){
        	    $scope.activePath = $location.path();
        	});
        });
        
        app.run(function($rootScope, $location,$cookieStore,$timeout, $http, Timer){
        	
        	$rootScope.$on('$locationChangeStart', function (event) {
                var userCookie = $cookieStore.get("currentUser");
                var restrictedPage = $.inArray($location.path(), ['/signin', '/signup']) === -1;                 
                
                if (restrictedPage && angular.isUndefined(userCookie)) {
                	Timer.stopTimer();
                	$location.path('/signin');
                }
        	});

        	$rootScope.setCurrentUser = function(user){
        		$cookieStore.put("currentUser", user);
        	};
        	
        	$rootScope.getCurrentUser = function(){
        		return $cookieStore.get("currentUser");
        	};
        	
        	$rootScope.removeCurrentUser = function(){
        		$cookieStore.remove("currentUser");
        	};
        	
        	$rootScope.setCurrentTest = function(exam){
        		$cookieStore.put("currentTest", exam);
        	};
        	
        	$rootScope.getCurrentTest = function(){
        		return $cookieStore.get("currentTest");
        	};
        	
        	$rootScope.removeCurrentTest = function(){
        		$cookieStore.remove("currentTest");
        	};
        	
        	$rootScope.status = function(type, prefix, message){
        		$rootScope.status.type = type;
        		$rootScope.status.prefix = prefix;
        		$rootScope.status.message = message;
        	};
        	
        	$rootScope.error = function(message){
        		$rootScope.status('error', 'Error!', message);
        	};
        	
        	$rootScope.ok = function(message){
        		$rootScope.status('success', 'Success!', message);
        	};
        	
        	$rootScope.warning = function(message){
        		$rootScope.status('warning', 'Warning!', message);
        	};
        	
        	$rootScope.info = function(message){
        		$rootScope.status('info', 'Info!', message);
        	};
        	
        	$rootScope.clearErrors = function(){
        		$rootScope.status.message = null;
        	};
        	
     });
      
     return angularAMD.bootstrap(app);
});

