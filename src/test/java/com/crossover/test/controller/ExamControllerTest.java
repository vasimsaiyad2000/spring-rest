package com.crossover.test.controller;


import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.crossover.test.dto.ExamDTO;
import com.crossover.test.exception.AbstractExceptionHandler;
import com.crossover.test.mapper.ExamMapper;
import com.crossover.test.persistence.Exam;
import com.crossover.test.service.ExamService;
import com.crossover.test.service.UserExamService;


public class ExamControllerTest extends AbstractControllerTest{

	@Mock
	private ExamService examService;
	
	@Mock
	private ExamMapper examMapper;
	
	@Mock
	private UserExamService userExamService;
	
	@InjectMocks
	private ExamController examController;
	
	@Before
	public void setUp() {
		super.setUp();
		mockMvc = MockMvcBuilders.standaloneSetup(examController)
				.setControllerAdvice(new AbstractExceptionHandler())
				.build();
	}
	
	@Test
	public void findById_Found_ShouldReturnFoundExam() throws Exception{
		Exam exam = mockExam();
		ExamDTO examDTO = mockExamDTO(exam);

		when(examService.getById(1L)).thenReturn(exam);
		when(examMapper.convertToExamDTO(exam)).thenReturn(examDTO);
		
		mockMvc.perform(get("/api/exam/{id}", exam.getId()))
			.andExpect(status().isOk())
			.andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$.id", is(examDTO.getId().intValue())))
			.andExpect(jsonPath("$.title", is(examDTO.getTitle())))
			.andExpect(jsonPath("$.description", is(examDTO.getDescription())))
            .andDo(print());
		
		verify(examService, times(1)).getById(exam.getId());
		verifyNoMoreInteractions(examService);
	}
	
	@Test
	public void findById_NotFound_ShouldReturnHttpStatus404() throws Exception{
		
		when(examService.getById(1L)).thenReturn(null);
		
		mockMvc.perform(get("/api/exam/{id}", 1L))
			.andExpect(status().isNotFound())
            .andDo(print());
		
		verify(examService, times(1)).getById(1L);
		verifyNoMoreInteractions(examService);
	}
	
	private Exam mockExam(){
		
		Exam exam  = new Exam();
		
		exam.setId(1L);
		exam.setTitle("Test Exam");
		exam.setDescription("This is test exam");
		exam.setActive(true);
		exam.setDuration(3600L);
		
		return exam;
	}
	
	private ExamDTO mockExamDTO(Exam exam){
		
		ExamDTO examDTO = new ExamDTO();
		examDTO.setId(exam.getId());
		examDTO.setDuration(exam.getDuration());
		examDTO.setTitle(exam.getTitle());
		examDTO.setDescription(exam.getDescription());
		
		return examDTO;
	}
}
