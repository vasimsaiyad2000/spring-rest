package com.crossover.test.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.crossover.test.dto.UserAnswerDTO;
import com.crossover.test.exception.AbstractExceptionHandler;
import com.crossover.test.persistence.Choice;
import com.crossover.test.persistence.Question;
import com.crossover.test.persistence.User;
import com.crossover.test.persistence.UserQuestionAnswer;
import com.crossover.test.service.UserAnswerService;


public class UserAnswerControllerTest extends AbstractControllerTest{

	@Mock
	private UserAnswerService userAnswerService;
	
	
	@InjectMocks
	private UserAnswerController controller;
	
	@Before
	public void setUp() {
		super.setUp();
		mockMvc = MockMvcBuilders.standaloneSetup(controller)
				.setControllerAdvice(new AbstractExceptionHandler())
				.build();
	}
	

	@Test
	public void save_UserAnswer_ShouldAddAndReturnAddedAnswer() throws Exception{
		
		UserAnswerDTO answer = mockUserAnswer();
	
		UserQuestionAnswer uqa = new UserQuestionAnswer();
		uqa.setChoice(new Choice(answer.getChoiceId()));
		uqa.setUser(new User(answer.getUserId()));
		uqa.setQuestion(new Question(answer.getQuestionId()));
		
		when(userAnswerService.saveAnswer(answer.getUserId(), answer.getQuestionId(), answer.getChoiceId())).thenReturn(uqa);
		
		mockMvc.perform(post("/api/answer/")
					.contentType(TestUtils.APPLICATION_JSON_UTF8)
					.content(TestUtils.convertObjectToJsonBytes(answer)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
				.andDo(print());
		
		verify(userAnswerService, timeout(1)).saveAnswer(uqa.getUser().getId(), uqa.getQuestion().getId(), uqa.getChoice().getId());
		verifyNoMoreInteractions(userAnswerService);
	}
	
	private UserAnswerDTO mockUserAnswer(){
		UserAnswerDTO userAnswerDTO = new UserAnswerDTO();
		userAnswerDTO.setChoiceId(1L);
		userAnswerDTO.setQuestionId(1L);
		userAnswerDTO.setUserId(1L);
		
		return userAnswerDTO;
	}
}
