package com.crossover.test.controller;


import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import java.util.ArrayList;
import java.util.List;

import javax.swing.text.AbstractDocument.Content;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.crossover.test.dto.QuestionDTO;
import com.crossover.test.dto.QuestionRequestDTO;
import com.crossover.test.exception.AbstractExceptionHandler;
import com.crossover.test.mapper.QuestionMapper;
import com.crossover.test.mapper.UserAnswerMapper;
import com.crossover.test.persistence.Exam;
import com.crossover.test.persistence.Question;
import com.crossover.test.service.QuestionService;

public class QuestionControllerTest extends AbstractControllerTest{

	@Mock
	private QuestionService questionService;
	
	@Mock
	private QuestionMapper questionMapper;
	
	@Mock
	private UserAnswerMapper userAnswerMapper;
	
	@InjectMocks
	private QuestionController questionController;
		
	@Before
	public void setUp() {
		super.setUp();
		mockMvc = MockMvcBuilders.standaloneSetup(questionController)
				.setControllerAdvice(new AbstractExceptionHandler())
				.build();
	}
	
	@Test
	public void findByExamId_Found_ShouldReturnFoundQuestion() throws Exception{

		Question q = mockQuestion();
		
		QuestionRequestDTO request = new QuestionRequestDTO();
		request.setExamId(1L);
		request.setUserId(1L);
		request.setQuestionNumber(1);
		
		QuestionDTO qDTO = mockQuestionDTO(q);
		
		when(questionService.getByExam(request.getExamId(), request.getUserId(), request.getQuestionNumber() - 1)).thenReturn(q);
		when(questionService.getCountByExam(request.getExamId())).thenReturn(1L);
		when(questionMapper.convertToQuestionDTO(q)).thenReturn(qDTO);

		mockMvc.perform(post("/api/question/")
					.contentType(TestUtils.APPLICATION_JSON_UTF8)
					.content(TestUtils.convertObjectToJsonBytes(request)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.id", is(qDTO.getId().intValue())))
				.andExpect(jsonPath("$.text", is(qDTO.getText())))
				.andDo(print());

		verify(questionService, times(1)).getByExam(1L, 1L, 0);
	}
	
	
	@Test
	public void findByExamId_BadQuestionNumber_ShouldReturnBadRequest() throws Exception{

		QuestionRequestDTO request = new QuestionRequestDTO();
		request.setExamId(1L);
		request.setUserId(1L);
		request.setQuestionNumber(0);

		mockMvc.perform(post("/api/question/")
					.content(TestUtils.convertObjectToJsonBytes(request))
					.contentType(TestUtils.APPLICATION_JSON_UTF8))
				.andExpect(status().isBadRequest())
				.andDo(print());

		verifyZeroInteractions(questionService);
	}

	private Question mockQuestion(){
		Question q = new Question();
		q.setExam(new Exam(1L));
		q.setPoint(10L);
		q.setId(1L);
		q.setText("This is test question");
		
		return q;
	}
	
	private QuestionDTO mockQuestionDTO(Question q){
		
		QuestionDTO  qDTO = new QuestionDTO();
		qDTO.setCurrentQuestion(1);
		qDTO.setNextQuestion(2);
		qDTO.setLastQuestion(true);
		qDTO.setId(q.getId());
		qDTO.setPoint(q.getPoint());
		qDTO.setText(q.getText());
		
		return qDTO;
	}
}
