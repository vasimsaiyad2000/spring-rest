
### NOTE ###

1. Please read **Howto** and **README** file given under **src/main/webapp/doc** folder to configure application and database in your local enviornment.

2. Please look into **src/main/webapp/video** folder that contains the application demo video.

3. Please look into **Design.docx** file under **src/main/webapp/doc** folder that contains all the design diagrams.

4. Please restore mysql tables into your local mysql server from given script under **src/main/webapp/sql** folder.